module Vagrant
  module LXC
    module Action
      class SetVagrantUid

        def initialize(app, env)
          @app = app
          @env = env
          @logger = Log4r::Logger.new("vagrant::lxc::action::set_vagrant_uid")
        end

        def call(env)
          check_and_amend_vagrant_uid(env)
          @app.call(env)
        end

        def check_and_amend_vagrant_uid(env)
          driver = env[:machine].provider.driver
          username = env[:machine].config.ssh.username || env[:machine].config.ssh.default.username

          olduid = driver.attach('id', '-u', username).strip
          newuid = Process.uid.to_s

          if olduid != newuid
            @logger.info("Changing #{username} user's uid to #{newuid}")
            begin
              # TODO check that home/vagrant is part of the root fs, as a belts and braces check
              driver.attach 'usermod', '-u', newuid, username
              driver.attach 'bash', '-e', '-o', 'pipefail', '-c', "find /home/#{username} -xdev -user #{olduid} | xargs chown -h #{newuid} /home/#{username}"
            rescue LXC::Errors::ExecuteError => e
              @logger.warn("Failed to change #{username} user's uid to #{newuid} - #{e.stderr}}")
              env[:ui].warn(I18n.t("vagrant.actions.lxc.changing_uid_failed", {user: username, error: e.stderr}))
            end
          end
        end
      end
    end
  end
end
